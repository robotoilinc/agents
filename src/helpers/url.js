// Possible websites
export const WEBSITE_1688 = '1688';
export const WEBSITE_TAOBAO = 'taobao';
export const WEBSITE_TMALL = 'tmall';
export const WEBSITE_YUPOO = 'yupoo';
export const WEBSITE_WEIDIAN = 'weidian';
export const WEBSITE_XIANYU = 'xianyu';
export const WEBSITE_UNKNOWN = 'unknown';

/**
 * @internal
 * @param url {string}
 * @returns {string}
 */
const ensureNonEncodedURL = (url) => {
  if (url === decodeURIComponent(url || '')) {
    return url;
  }

  // Grab the encoded URL
  const encodedURL = new URL(url).searchParams.get('url') || '';
  if (encodedURL.length === 0) {
    return url;
  }

  // Decode said encoded URL
  const decodedURL = decodeURIComponent(encodedURL);
  if (decodedURL.length === 0) {
    return url;
  }

  return decodedURL;
};

/**
 * @param url {string}
 * @returns {boolean}
 */
export const isUrl = (url) => { try { return Boolean(new URL(url)); } catch (e) { return false; } };

/**
 * @param originalUrl {string}
 * @param website {string}
 * @returns {string}
 */
export const cleanPurchaseUrl = (originalUrl, website) => {
  const url = ensureNonEncodedURL(originalUrl);

  const idMatches = url.match(/[?&]id=(\d+)|[?&]itemID=(\d+)|\/?[albums]\/(\d+)|offer\/(\d+)/i);
  const authorMatches = url.match(/https?:\/\/(.+)\.x\.yupoo\.com/);

  if (website === WEBSITE_TAOBAO && idMatches[1].length !== 0) {
    return `https://item.taobao.com/item.htm?id=${idMatches[1]}`;
  }

  if (website === WEBSITE_TMALL && idMatches[1].length !== 0) {
    return `https://detail.tmall.com/item.htm?id=${idMatches[1]}`;
  }

  if (website === WEBSITE_XIANYU && idMatches[1].length !== 0) {
    return `https://2.taobao.com/item.htm?id=${idMatches[1]}`;
  }

  if (website === WEBSITE_WEIDIAN && idMatches[2].length !== 0) {
    return `https://weidian.com/item.html?itemID=${idMatches[2]}`;
  }

  if (website === WEBSITE_YUPOO && idMatches[3].length !== 0 && authorMatches[1].length !== 0) {
    return `https://${authorMatches[1]}.x.yupoo.com/albums/${idMatches[3]}`;
  }

  if (website === WEBSITE_1688 && idMatches[4].length !== 0) {
    return `https://detail.1688.com/offer/${idMatches[4]}.html`;
  }

  // Just return the original URL with some clean up
  return originalUrl.replace('http://', 'https://').replace('?uid=1', '').trim();
};

/**
 * @param originalUrl {string}
 * @returns {string}
 */
export const determineWebsite = (originalUrl) => {
  if (originalUrl.indexOf('1688.com') !== -1) {
    return WEBSITE_1688;
  }

  // Check more specific taobao first
  if (originalUrl.indexOf('market.m.taobao.com') !== -1 || originalUrl.indexOf('2.taobao.com') !== -1) {
    return WEBSITE_XIANYU;
  }

  if (originalUrl.indexOf('taobao.com') !== -1) {
    return WEBSITE_TAOBAO;
  }

  if (originalUrl.indexOf('detail.tmall.com') !== -1) {
    return WEBSITE_TMALL;
  }

  if (originalUrl.indexOf('weidian.com') !== -1 || originalUrl.indexOf('koudai.com') !== -1) {
    return WEBSITE_WEIDIAN;
  }

  if (originalUrl.indexOf('yupoo.com') !== -1) {
    return WEBSITE_YUPOO;
  }

  return WEBSITE_UNKNOWN;
};
