import BaseTao from './basetao/agent';
import CSSBuy from './cssbuy/agent';
import WeGoBuy from './wegobuy/agent';

/**
 * @param hostname {string}
 *
 * @returns {BaseTao|CSSBuy|WeGoBuy|null}
 */
export default function getAgent(hostname) {
  const agents = [new BaseTao(), new CSSBuy(), new WeGoBuy()];

  let agent = null;
  Object.values(agents).forEach((value) => {
    if (agent == null && value.supports(hostname)) {
      agent = value;
    }
  });

  return agent;
}
