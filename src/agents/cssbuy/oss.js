/* eslint-disable no-return-await */
export default class OSS {
  constructor() {
    this.setup = false;
    this.window = window.unsafeWindow;
  }

  async build() {
    if (this.setup) {
      return this;
    }

    // Try and build the OSS client
    try {
      // Grab the OSS client
      const WindowOSS = await this._waitForValue('OSS');

      // Build the config for the bucket
      const config = {
        region: await this._waitForValue('c_region'),
        accessKeyId: await this._waitForValue('c_accessid'),
        accessKeySecret: await this._waitForValue('c_accesskey'),
        bucket: await this._waitForValue('c_bucket'),
        endpoint: `https://${await this._waitForValue('c_region')}.aliyuncs.com/`,
      };

      // Log the config, for ease of use
      Logger.info('OSS config build', config);

      // Set up the bucket for easy use
      this.window.client = new WindowOSS.Wrapper(config);

      // Mark as ready
      this.setup = true;
    } catch (e) {
      throw new Error(e);
    }

    return this;
  }

  /**
   * @param {string} orderId
   *
   * @return Promise<object>
   */
  async list(orderId) {
    if (this.setup === false) {
      throw new Error('OSS is not setup, so cannot be used');
    }

    return await this.window.client.list({
      'max-keys': 100,
      prefix: `o/${orderId}/`,
    });
  }

  _waitForValue(value) {
    return new Promise((resolve) => {
      // Check if the element already exists
      if (this.window[value]) {
        resolve(this.window[value]);

        return;
      }

      const _waitForGlobal = () => {
        if (this.window[value]) {
          resolve(this.window[value]);

          return;
        }

        // Wait until we have it
        setTimeout(() => { _waitForGlobal(value, resolve); }, 100);
      };

      // It doesn't so, so let's start waiting for it
      _waitForGlobal(value, resolve);
    });
  }
}
