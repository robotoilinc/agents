import { cleanPurchaseUrl, determineWebsite } from '../../helpers/url';
import { removeWhitespaces, truncate } from '../../helpers/utils';

export default class CSSBuyElement {
  constructor($element) {
    this.element = $element;

    // Create empty array for images
    this.imageUrls = [];

    // Temporary items
    const parentTableEntry = $element.parentsUntil('tbody');
    const itemLink = parentTableEntry.find('td.tabletd3 > a');
    const splitText = parentTableEntry.find('td.tabletd3 > span:nth-child(3)').html().split('<br>');

    // Order details
    this.orderId = this.element.parent().attr('data-id');

    // Item name
    this.title = truncate(removeWhitespaces(itemLink.text()), 255);

    // Purchase details
    this.website = determineWebsite(itemLink.attr('href'));
    this.purchaseUrl = cleanPurchaseUrl(itemLink.attr('href'), this.website);

    // Item price
    this.itemPrice = `CNY ${removeWhitespaces(parentTableEntry.find('td:nth-child(4) > span:nth-child(1)').text())}`;

    // Freight price
    this.freightPrice = `CNY ${removeWhitespaces(parentTableEntry.find('td:nth-child(5) span').text())}`;

    // Item weight
    const weight = removeWhitespaces(parentTableEntry.find('td:nth-child(7) span').text());
    this.weight = weight.length !== 0 ? `${weight} gram` : null;

    // Item sizing and color (if any)
    this.color = null;
    this.sizing = null;

    try {
      if (splitText.length === 1) {
        let color = splitText[0].split(' : ')[1];
        color = (typeof color !== 'undefined' && color !== '-' && color.toLowerCase() !== 'no') ? truncate(color, 255) : '';
        this.color = color.length !== 0 ? color : null;
      } else if (splitText.length === 2) {
        let sizing = (splitText[0].split(' : ')[1]);
        sizing = (typeof sizing !== 'undefined' && sizing !== '-' && sizing.toLowerCase() !== 'no') ? truncate(sizing, 255) : '';
        this.sizing = sizing.length !== 0 ? sizing : null;

        let color = (splitText[1].split(' : ')[1]);
        color = (typeof color !== 'undefined' && color !== '-' && color.toLowerCase() !== 'no') ? truncate(color, 255) : '';
        this.color = color.length !== 0 ? color : null;
      } else if (splitText.length !== 0) {
        this.sizing = splitText.join('\n');
      }
    } catch (e) {
      Logger.info('Could not figure out sizing/color', e);
    }

    // Set at a later date, if ever
    this.albumId = null;
  }

  /**
   * @return {string}
   */
  get albumUrl() {
    return `https://imgur.com/a/${this.albumId}`;
  }
}
