import { cleanPurchaseUrl, determineWebsite } from '../../helpers/url';
import { removeWhitespaces, truncate } from '../../helpers/utils';

export default class BaseTaoElement {
  constructor($element, data) {
    this.element = $element;
    this.data = data;

    // Set the order id
    this.orderId = data.oid;

    // Item name
    this.title = truncate(removeWhitespaces(data.goodsname), 255);

    // Item and shipping prices
    this.itemPrice = `CNY ${data.goodsprice}`;
    this.freightPrice = `CNY ${data.sendprice}`;

    // URL related stuff
    this.url = data.goodsurl;
    this.website = determineWebsite(this.url);

    // QC images location
    this.qcImagesUrl = `https://www.basetao.com/best-taobao-agent-service/purchase/order_img/${data.oid}.html`;

    // Item sizing (if any)
    let sizing = removeWhitespaces(data.goodssize);
    sizing = (sizing !== '-' && sizing.toLowerCase() !== 'no') ? truncate(sizing, 255) : '';
    this.sizing = sizing.length !== 0 ? sizing : null;

    // Item color (if any)
    let color = removeWhitespaces(data.goodscolor);
    color = (color !== '-' && color.toLowerCase() !== 'no') ? truncate(color, 255) : '';
    this.color = color.length !== 0 ? color : null;

    // Item weight
    const weight = removeWhitespaces(data.orderweight);
    this.weight = weight.length !== 0 ? `${weight} gram` : null;

    // Image url storage, for later
    this.imageUrls = [];

    // Set at a later date, if ever
    this.albumId = null;
  }

  /**
   * @return {string}
   */
  get albumUrl() {
    return `https://imgur.com/a/${this.albumId}`;
  }

  /**
   * @param imageUrls {string[]}
   */
  set images(imageUrls) {
    this.imageUrls = imageUrls;
  }

  /**
   * @returns {string}
   */
  get purchaseUrl() {
    return cleanPurchaseUrl(this.url, this.website);
  }
}
