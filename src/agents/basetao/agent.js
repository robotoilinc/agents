import { ConfirmDialog, Snackbar } from '../../helpers/snackbar';
import { ImgurSlowdownError } from '../../errors/ImgurSlowdownError';
import { WEBSITE_UNKNOWN } from '../../helpers/url';
import { WebpToJpg, toDataURL } from '../../helpers/utils';
import { elementReady } from '../../helpers/elementReady';
import BaseTaoElement from './element';
import Imgur, { ImgurIcon, Loading } from '../../clients/imgur';
import QC from '../../clients/qc';

export default class BaseTao {
  constructor() {
    this.setup = false;
  }

  /**
   * @param hostname {string}
   * @returns {boolean}
   */
  supports(hostname) {
    return hostname.includes('basetao.com');
  }

  /**
   * @returns {string}
   */
  name() {
    return 'BaseTao';
  }

  /**
   * @param client {Promise<SwaggerClient>}
   * @returns {Promise<BaseTao>}
   */
  async build(client) {
    // If already build before, just return
    if (this.setup) {
      return this;
    }

    // Get the username
    let username = $('[aria-labelledby="profileDropdown"] a:first').text();
    if (typeof username === 'undefined' || username == null || username === '') {
      Snackbar('You need to be logged in to use this extension.');
      throw new Error('You need to be logged in to use this extension.');
    }

    // Trim the username
    username = username.trim();

    // Hash the username (and add an extra space, since this was a bug in the past)
    const userHash = SparkMD5.hash(`${username} `);

    // Ensure we know who triggered errors
    Sentry.setUser({ id: userHash, username });

    // Build all the clients
    this.imgurClient = new Imgur(GM_info.script.version, GM_config, this.name());
    this.qcClient = new QC(GM_info.script.version, await client, userHash, username, this.name());

    // Mark that this agent has been set up
    this.setup = true;

    return this;
  }

  /**
   * @return {Promise<void>}
   */
  async process() {
    if (this.setup === false) {
      throw new Error('Agent is not setup, so cannot be used');
    }

    // Make copy of the current this, so we can use it later
    const agent = this;

    // Get the container (unsafe, because we want the actual jQuery table)
    const $container = window.unsafeWindow.jQuery('#table').first();

    // Start processing once the table has been loaded
    elementReady('.details-tr i.bi.bi-image-fill').then(() => {
      const rowData = $container.bootstrapTable('getData');
      $container.find('i.bi.bi-image-fill').each(function () {
        const $element = $(this);
        const orderId = $element.parents('td').find('[data-row]').data('row');
        agent._buildElement($element, rowData.find(agent._getRowData(orderId)));
      });

      $container.on('load-success.bs.table', (event, data) => {
        $container.find('i.bi.bi-image-fill').each(function () {
          const $element = $(this);
          const orderId = $element.parents('td').find('[data-row]').data('row');
          agent._buildElement($(this), data.rows.find(agent._getRowData(orderId)));
        });
      });

      // Ensure tooltips
      $container.tooltip({ selector: '.qc-tooltip' });
    });
  }

  /**
   * @private
   * @param $this
   * @param {RowData} data
   * @return {Promise<BaseTaoElement>}
   */
  async _buildElement($this, data) {
    const element = new BaseTaoElement($this, data);
    const $imageIcon = element.element.parents('a').first();

    // Append download button if enabled
    if (GM_config.get('showImagesDownloadButton')) {
      const $download = $('<span style="cursor: pointer;padding-left: 5px;" class="bi bi-download text-orange" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Download all photos"></span>');
      $download.on('click', () => this._downloadHandler($download, element));
      $imageIcon.parent().append($download);
    }

    // This plugin only works for certain websites, so check if element is supported
    if (element.website === WEBSITE_UNKNOWN) {
      const $upload = $(`<div><span style="cursor: pointer;"><img src="${ImgurIcon}" alt="Create a basic album"></span></div>`);
      $upload.find('span').first().after($('<span class="qc-marker qc-tooltip" style="cursor:help;margin-left:5px;color:red;font-weight: bold;" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Not a supported URL, but you can still create an album. The QC\'s are not stored and you\'ll have to create a new album if you lose the link.">✖</span>'));
      $upload.on('click', () => {
        this._uploadHandler(element);
      });

      $this.parents('td').first().append($upload);

      return element;
    }

    const $loading = $(`<div><span class="qc-tooltip" style="cursor: wait;" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Loading..."><img src="${Loading}" alt="Loading..."></span></div>`);
    $this.parents('td').first().append($loading);

    // Define upload object
    const $upload = $(`<div><span class="qc-marker qc-tooltip" style="cursor: pointer;" style="cursor: wait;" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Upload your QC"><img src="${ImgurIcon}" alt="Upload your QC"></span></div>`);

    // If we couldn't talk to FR:Reborn, assume everything is dead and use the basic uploader.
    const albumId = await this.qcClient.existingAlbumByOrderId(element);
    if (albumId === '-1') {
      $upload.find('span').first().html($('<span class="qc-marker qc-tooltip" style="cursor:help;color:red;font-weight: bold;" data-bs-toggle="tooltip" data-bs-placement="bottom" title="FR:Reborn returned an error or could not load your album.">⚠️</span>'));

      $this.parents('td').first().append($upload);
      $loading.remove();

      return element;
    }

    // Have you ever uploaded a QC? If so, link to that album
    const $image = $upload.find('img');
    if (albumId !== null && albumId !== '-1') {
      $upload.find('span').first().after($('<span class="qc-marker qc-tooltip" style="cursor:help;margin-left:5px;color:green;font-weight: bold;" data-bs-toggle="tooltip" data-bs-placement="bottom" title="You have uploaded a QC">✓</span>'));
      $image.wrap(`<a href='${element.albumUrl}' target='_blank' data-bs-toggle="tooltip" data-bs-placement="bottom" title='Go to album'></a>`);
      $image.removeAttr('title');

      $this.parents('td').first().append($upload);
      $loading.remove();

      return element;
    }

    // Has anyone ever uploaded a QC, if not, show a red marker
    const exists = await this.qcClient.exists(element.purchaseUrl);
    if (!exists) {
      $upload.find('span').first().after($('<span class="qc-marker qc-tooltip" style="cursor:help;margin-left:5px;color:red;font-weight: bold;" data-bs-toggle="tooltip" data-bs-placement="bottom" title="No QC in database, please upload.">(!)</span>'));
      $upload.on('click', () => {
        this._uploadHandler(element);
      });

      $this.parents('td').first().append($upload);
      $loading.remove();

      return element;
    }

    // A previous QC exists, but you haven't uploaded yours yet, show orange marker
    $upload.find('span').first().after($('<span class="qc-marker qc-tooltip" style="cursor:help;margin-left:5px;color:orange;font-weight: bold;" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Your QC is not yet in the database, please upload.">(!)</span>'));
    $upload.on('click', () => {
      this._uploadHandler(element);
    });

    $this.parents('td').first().append($upload);
    $loading.remove();

    return element;
  }

  /**
   * @private
   * @param element {BaseTaoElement}
   * @returns {Promise<void>}
   */
  async _uploadToImgur(element) {
    if (this.setup === false) {
      throw new Error('Agent is not setup, so cannot be used');
    }

    if (element.imageUrls.length === 0) {
      Snackbar(`No pictures found for order ${element.orderId}, skipping,..`);
      return;
    }

    const $processing = $(`<span style="cursor: wait;"><img src="${Loading}" alt="Processing..."></span>`);
    const $base = element.element.parents('td').first().find('div').first();
    $base.after($processing).hide();

    // Start the process
    Snackbar(`Pictures for '${element.orderId}' are being uploaded...`);

    // Temp store deleteHash
    let deleteHash;

    try {
      // Create the album
      const response = await this.imgurClient.CreateAlbum(element);
      if (typeof response === 'undefined' || response == null) {
        return;
      }

      // Extract and build information needed
      deleteHash = response.data.deletehash;
      const albumId = response.data.id;

      // Upload all QC images
      const promises = [];
      $.each(element.imageUrls, (key, imageUrl) => {
        // Convert to base64, since Imgur cannot access our images
        promises.push(toDataURL(imageUrl).then(async (data) => {
          // Store our base64 and if the file is WEBP, convert it to JPG
          let base64Image = data;
          if (base64Image.indexOf('image/webp') !== -1) {
            base64Image = await WebpToJpg(base64Image);
          }

          // Remove the unnecessary `data:` part
          const cleanedData = base64Image.replace(/(data:image\/.*;base64,)/, '');

          // Upload the image to the album
          return this.imgurClient.AddBase64ImageToAlbum(cleanedData, deleteHash, element.purchaseUrl);
        }));
      });

      // Wait until everything has been tried to be uploaded
      await Promise.all(promises);

      // Set albumId in element, so we don't upload it again (when doing a pending haul upload)
      element.albumId = albumId; // eslint-disable-line no-param-reassign

      // Tell the user it was uploaded and open the album in the background
      Snackbar('Pictures have been uploaded!', 'success');
      GM_openInTab(element.albumUrl, true);

      // Tell QC Suite about our uploaded QC's (if it's supported)
      if (element.website !== WEBSITE_UNKNOWN) {
        this.qcClient.uploadQc(element, albumId);
      }

      // Wrap the logo in a href to the new album
      const $image = $base.find('img');
      $image.wrap(`<a href='${element.albumUrl}' target='_blank' data-bs-toggle="tooltip" data-bs-placement="bottom" title='Go to album'></a>`);
      $image.removeAttr('title');

      // Remove processing
      $processing.remove();

      // Update the marker
      const checkMarkMessage = element.website !== WEBSITE_UNKNOWN ? 'You have uploaded a QC' : 'Album has been created';
      const $qcMarker = $base.find('.qc-marker:not(:first-child)').first();
      $qcMarker.attr('title', checkMarkMessage)
        .css('cursor', 'help')
        .css('color', 'green')
        .text('✓');

      // Remove the click handler
      $base.off();

      // Show it again
      $base.show();
    } catch (err) {
      // Remove the created album
      this.imgurClient.RemoveAlbum(deleteHash);

      // Reset the button
      $processing.remove();
      $base.show();

      // Show the error
      Snackbar(err.message, 'error');

      // If it's the slow down error, don't log it
      if (err instanceof ImgurSlowdownError) {
        return;
      }

      // Log the error
      Sentry.captureException(err);
      Logger.error(err);
    }
  }

  /**
   * @private
   * @param $download
   * @param element {BaseTaoElement}
   */
  async _downloadHandler($download, element) {
    if (this.setup === false) {
      throw new Error('Agent is not setup, so cannot be used');
    }

    if (!await ConfirmDialog()) {
      return;
    }

    // Remove button so people don't do dumb shit
    $download.remove();

    // Go to the QC pictures URL, grab all image sources and upload the element
    await $.get(element.qcImagesUrl).then(async (data) => {
      if (data.indexOf('long time no operation ,please sign in again') !== -1) {
        Snackbar('You are no longer logged in, reloading page....', 'warning');
        Logger.info('No longer logged in, reloading page for user...');
        window.location.reload();

        return null;
      }

      Snackbar('Zipping images, this might take a while....', 'info');

      // Create a zip file writer
      const zipWriter = new zip.ZipWriter(new zip.Data64URIWriter('application/zip'));

      // Download all the images and add to the zip
      const promises = [];
      $('<div/>').html(data).find('.card > img').each(function () {
        const src = $(this).attr('src');
        promises.push(new Promise((resolve) => toDataURL(src)
          .then((dataURI) => zipWriter.add(src.substring(src.lastIndexOf('/') + 1), new zip.Data64URIReader(dataURI)))
          .then(() => resolve())));
      });

      // Wait for all images to be added to the ZIP
      await Promise.all(promises);

      // Close the ZipWriter object and download to computer
      saveAs(await zipWriter.close(), `${element.orderId}.zip`);

      Snackbar(`Downloading ${element.orderId}.zip`, 'success');

      return null;
    }).catch((err) => {
      Snackbar(`Could not get all images for order ${element.orderId}`);
      Logger.error(`Could not get all images for order ${element.orderId}`, err);
    });
  }

  /**
   * @private
   * @param element {BaseTaoElement}
   * @returns {Promise<void>}
   */
  async _uploadHandler(element) {
    if (this.setup === false) {
      throw new Error('Agent is not setup, so cannot be used');
    }

    // Go to the QC pictures URL, grab all image sources and upload the element
    await $.get(element.qcImagesUrl).then(async (data) => {
      if (data.indexOf('long time no operation ,please sign in again') !== -1) {
        Snackbar('You are no longer logged in, reloading page....', 'warning');
        Logger.info('No longer logged in, reloading page for user...');
        window.location.reload();

        return null;
      }

      // Add all image urls to the element
      $('<div/>').html(data).find('main div.container.container img').each(function () {
        element.imageUrls.push($(this).attr('src'));
      });

      // Finally go and upload the order
      return this._uploadToImgur(element);
    }).catch((err) => {
      Snackbar(`Could not get all images for order ${element.orderId}`);
      Logger.error(`Could not get all images for order ${element.orderId}`, err);
    });
  }

  /**
   * @private
   * @param orderId
   */
  _getRowData(orderId) {
    return (item) => Number(item.oid) === Number(orderId);
  }
}
