import { ImgurError } from '../errors/ImgurError';
import { ImgurSlowdownError } from '../errors/ImgurSlowdownError';

export const ImgurIcon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAA7EAAAOxAGVKw4bAAACl0lEQVQ4jX2TS0jUURSHv3v/d9TRMh0tzYrQiF7qpgijaFERRhlkxSwyiggXKdEiCjLBCi10UVirau0mCgzMVQQ96EHv1GxRWWBoKTbK2H+c+2gx6ZRIZ3U238c59/yuAFjQebQSqAeKcSj+VwINdAHn+re13hJ/4Ju4/2IziQB2K6B+JlgKQSglg0wVJF2lEhAeFseAH2HQj4ADBPUKKJ6C4nBxdRVrQ0VkBjL4aWOMuTi/nEHjkEKyVGWy42EzX8eHAYrV5M7Ot7jLETa1r+TE4F0ej39DegrpKYSnkCrRN4fWsSiYkxA4lJyEo40DzPmeQnjPWc5kb2RlaghrNNZonNFYneits0ghkqs63xK7MITpjVFVtZneD1958eA9+7JLEuA0SQoS30wkBbp5hKZD+ykpLaSxqY0zDQdIX59LXV9nEvxLMksEGNX+lEB5Hw3OOYJpqVxurWVBxWJqP9xAS4n0EpGQgP0DZMkURuLR5ATRqM/J49ep2FlGwfaFHOluI6ZjOBPHGo2whtPzk28SkmkMx8b+OrcTOOeYm5tF11g/Ezo2NbK0hguLy5n7RnMpdyvr0gsYjUcxLhkcCYJdlRtoPNdGub+McP4anNEIa2lZspPYvSGuXuvgYPg8zTmbeT7y+d9AZgUr40IKZZwhLz+b9o4Gbqtuls+ez8T9CLU1V/D9CYy1hMpyEMeyMJ6bjLOWONFVUlqIcIL8vGzKt9RR9DKDwfZvVB++REtLNaWlRQRWBDE1s5Nworq8oFr1Y3BgJJyaFmBveCOPH/Vwp+MZkUiUL33fef3qE32BYdJP5SHS5PTPVOP5uud9UK16Z7Rd9vRJb64xTlrjcM4xOjrOr4WWYN28JCzQCN4CNf3bWm/9BkZ7QCBmf01HAAAAAElFTkSuQmCC';
export const Loading = 'data:image/gif;base64,R0lGODlhEAAQAPMAAP////r6+paWlr6+vnx8fIyMjOjo6NDQ0ISEhLa2tq6urvDw8MjIyODg4J6enqampiH/C05FVFNDQVBFMi4wAwEAAAAh+QQJCgAAACwAAAAAEAAQAAAETBDISau9NQjCiUxDYGmdhBCFkRUlcLCFOA3oNgXsQG2HRh0EAYWDIU6MGSSAR1G4ghRa7KjIUXCog6QzpRhYiC1HILsOEuJxGcNuTyIAIfkECQoAAAAsAAAAABAAEAAABGIQSGkQmzjLQkTTWDAgRGmAgMGVhAIESxZwBUMgSyAUATYQPIBg8OIQJwLCQbJkdjAlUCA6KfU0VEmyGWgenpNfcCAoEo6SmWtBYtCukxhAwQKeQAYWYgAHNZIFKBoMCHcTEQAh+QQJCgAAACwAAAAAEAAQAAAEWhDIOZejGDNysgyDQBAIGWRGMa7jgAVq0TUj0lEDUZxArvAU0a1nAAQOrsnIA1gqCZ6AUzI4nAxJwIEgyAQUhCQsjDmUCI1jDEhlrQrFV+ksGLApWwYz41jsIwAh+QQJCgAAACwAAAAAEAAQAAAEThDISau9IIQahiCEMGhCQxkFqBLFZ0pBWhzSkYIvMLAb/OGTBII2+QExSEBjuexhVgrKAZGgqKKTGGFgBc00Np71cVsVDJVo5ydyJt/wCAAh+QQJCgAAACwAAAAAEAAQAAAEWhDISau9OAxBiBjBtRRdSRTGpRRHeJBFOKWALAXkAKQNoSwWBgFRQAA4Q5DkgOwwhCXBYTJAdAQAopVhWSgIjR1gcLLVQrQbrBV4CcwSA8l0Alo0yA8cw+9TIgAh+QQJCgAAACwAAAAAEAAQAAAEWhDISau9WA5CxAhWMDDAwXGFQR0IgQRgWRBF7JyEQgXzIC2MFkc1MQkonMbAhyQ0Y5pBg0MREA4UwwnBWGhoUIAC55DwaAcQrIXATgyzE/bwCQ2sBGZmz7dEAAA7';

export default class Imgur {
  /**
   * @param version {string}
   * @param config {GM_config}
   * @param agent {string}
   * @constructor
   */
  constructor(version, config, agent) {
    this.version = version;
    this.agent = agent;

    if (config.get('imgurApi') === 'imgur') {
      this.headers = {
        authorization: `Client-ID ${config.get('imgurClientId')}`,
        'Content-Type': 'application/json',
      };
      this.host = config.get('imgurApiHost');

      return;
    }

    if (config.get('imgurApi') === 'rapidApi') {
      this.headers = {
        authorization: `Bearer ${config.get('rapidApiBearer')}`,
        'x-rapidapi-key': config.get('rapidApiKey'),
        'x-rapidapi-host': config.get('rapidApiHost'),
      };
      this.host = config.get('rapidApiHost');

      return;
    }

    throw new Error('Invalid Imgur API has been chosen');
  }

  /**
   * @param options
   * @returns {Promise<*|null>}
   */
  async CreateAlbum(options) {
    const requestData = {
      url: `https://${this.host}/3/album`,
      type: 'POST',
      headers: this.headers,
      data: JSON.stringify({
        title: options.title,
      }),
    };
    Sentry.addBreadcrumb({
      category: 'Imgur',
      message: 'Creating album',
      data: requestData,
    });

    Logger.debug('Creating album', requestData);

    return $.ajax(requestData).retry({ times: 3 }).catch((err) => {
      // Check if Imgur is being a bitch
      if (typeof err.responseJSON === 'undefined') {
        // Store request so we know what was asked
        this._storeRequestError(err);

        throw new ImgurError('Could not make an album, because Imgur is returning empty responses. Please try again later...', err);
      }

      this._handleImgurError(err);
    });
  }

  /**
   * @param base64Image {string}
   * @param albumDeleteHash {string}
   * @param purchaseUrl {string}
   * @returns {Promise<boolean>}
   */
  async AddBase64ImageToAlbum(base64Image, albumDeleteHash, purchaseUrl) {
    // First step, upload the image
    const requestData = {
      url: `https://${this.host}/3/image`,
      headers: this.headers,
      type: 'POST',
      data: JSON.stringify({
        album: albumDeleteHash,
        type: 'base64',
        image: base64Image,
        description: this._getImageDescription(purchaseUrl),
      }),
    };

    Logger.debug('Adding image to album', requestData);
    Sentry.addBreadcrumb({
      category: 'Imgur',
      message: 'Adding image to album',
      data: requestData,
    });

    await $.ajax(requestData).retry({ times: 3 }).catch((err) => {
      // Check if Imgur is being a bitch
      if (typeof err.responseJSON === 'undefined') {
        // Store request so we know what was asked
        this._storeRequestError(err);
      }

      this._handleImgurError(err);
    });
  }

  /**
   * @param imageUrl {string}
   * @param albumDeleteHash {string}
   * @param purchaseUrl {string}
   * @returns {Promise<*|null>}
   */
  async AddImageToAlbum(imageUrl, albumDeleteHash, purchaseUrl) {
    // First step, upload the image
    const requestData = {
      url: `https://${this.host}/3/image`,
      headers: this.headers,
      type: 'POST',
      data: JSON.stringify({
        album: albumDeleteHash,
        image: imageUrl,
        description: this._getImageDescription(purchaseUrl),
      }),
    };

    Logger.debug('Adding image to album', requestData);
    Sentry.addBreadcrumb({
      category: 'Imgur',
      message: 'Adding image to album',
      data: requestData,
    });

    await $.ajax(requestData).retry({ times: 3 }).catch((err) => {
      // Check if Imgur is being a bitch
      if (typeof err.responseJSON === 'undefined') {
        // Store request so we know what was asked
        this._storeRequestError(err);
      }

      this._handleImgurError(err);
    });
  }

  /**
   * @param deleteHash {string}
   */
  RemoveAlbum(deleteHash) {
    const requestData = {
      url: `https://${this.host}/3/album/${deleteHash}`,
      headers: this.headers,
      type: 'DELETE',
    };
    Sentry.addBreadcrumb({
      category: 'Imgur',
      message: 'Removing album',
      data: requestData,
    });

    $.ajax(requestData).retry({ times: 3 }).catch(() => {});
  }

  _getAlbumDescription() {
    return `Auto uploaded using FR:Reborn - ${this.agent} ${this.version}`;
  }

  /**
   * @param purchaseUrl {string}
   */
  _getImageDescription(purchaseUrl) {
    return purchaseUrl.length === 0 ? this._getAlbumDescription() : `W2C: ${purchaseUrl}`;
  }

  /**
   * @private
   * @param err {Error}
   */
  _storeRequestError(err) {
    Sentry.addBreadcrumb({
      category: 'Imgur',
      message: `Imgur returned: '${err.statusText}'`,
      data: err,
      level: Sentry.Severity.Error,
    });
  }

  /**
   * @private
   * @param err {Error}
   */
  _handleImgurError(err) {
    // If there is a server error, let the user now
    if (err.status === 503 || (err.responseJSON && err.responseJSON.status === 503)) {
      throw new ImgurError('Imgur is either down, over-capacity or you did too many requests. Try again later', err);
    }

    // If we uploaded too many files, re-throw as proper error (checking via old response setup, new response setup and a simple fallback)
    if (err.responseJSON && err.responseJSON.data && err.responseJSON.data.error && err.responseJSON.data.error.code === 429) {
      throw new ImgurSlowdownError(err.responseJSON.data.error.message, err);
    } else if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length === 1 && err.responseJSON.errors[0] && err.responseJSON.errors[0].code === 429) {
      throw new ImgurSlowdownError(err.responseJSON.errors[0].detail, err);
    } else if (err.status === 429 || (err.responseJSON && err.responseJSON.status === 429)) {
      throw new ImgurSlowdownError('Too Many Requests', err);
    }

    // Store request so we know what was asked
    this._storeRequestError(err);

    // If we have error data from Imgur, throw it (checking via the old response setup and the new one)
    if (err.responseJSON && err.responseJSON.data && err.responseJSON.data.error) {
      throw new ImgurError(`An error happened when uploading the image:\n${err.responseJSON.data.error.message}`, err);
    } else if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length !== 0 && err.responseJSON.errors[0].detail) {
      throw new ImgurSlowdownError(`An error happened when uploading the image:\n${err.responseJSON.errors[0].detail}`, err);
    }

    // If not, just show the full JSON
    throw new ImgurError(`An error happened when uploading the image:\n${err.responseJSON}`, err);
  }
}
