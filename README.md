# RobotOilInc - Agents extension

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/B0B7CTDVS)

Upload Taobao and Yupoo QCs from your favorite agent to Imgur + QC server.

## Currently supported agents:

- BaseTao
- CSSBuy
- Superbuy
- WeGoBuy

## Currently supported websites:

- Taobao
- Tmall
- Yupoo
- Weidian
